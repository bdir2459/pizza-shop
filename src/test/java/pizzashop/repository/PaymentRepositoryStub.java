package pizzashop.repository;

import pizzashop.model.Payment;

import java.util.ArrayList;
import java.util.List;

public class PaymentRepositoryStub {
    private List<Payment> paymentList;

    public PaymentRepositoryStub() {
        paymentList = new ArrayList<>();
    }

    public void add(Payment p) throws Exception {
        // default behaviour - does nothing
    }

    public List<Payment> getAll(){
        return new ArrayList<>(paymentList);
    }

    public int size() {
        return paymentList.size();
    }
}
