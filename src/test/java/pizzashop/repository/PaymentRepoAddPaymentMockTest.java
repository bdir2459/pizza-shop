package pizzashop.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.model.PaymentValidator;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentRepoAddPaymentMockTest {

    @Mock
    private PaymentValidator validator;

    @InjectMocks
    private PaymentRepository paymentRepo;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        paymentRepo.setValidator(validator);
        paymentRepo.clear();
    }


    /***** TESTS *****/
    @Test
    public void tc02_valid() {
        Payment p = new Payment(1, PaymentType.Cash, 50);
        assertEquals(0, paymentRepo.getAll().size());
        Mockito.when(validator.validate(p)).thenReturn(true);
//        Mockito.when(payment.toString()).thenReturn(payment.getTableNumber() + "," + payment.getType() + "," + payment.getAmount());
        paymentRepo.add(p);
        assertEquals(1, paymentRepo.getAll().size());
    }

    @Test
    public void tc02_non_valid() {
        Payment p = new Payment(1, PaymentType.Cash, -5);
        Mockito.when(validator.validate(p)).thenReturn(false);
        paymentRepo.add(p);
        assertEquals(0, paymentRepo.getAll().size());
    }
}
