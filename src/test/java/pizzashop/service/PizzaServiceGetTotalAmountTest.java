package pizzashop.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PizzaServiceGetTotalAmountTest {
    private PizzaService pizzaService;

    @BeforeEach
    void setUp() {
        PaymentRepository paymentRepo = new PaymentRepository();
        paymentRepo.clear();
        MenuRepository menuRepo = new MenuRepository();
        pizzaService = new PizzaService(menuRepo, paymentRepo);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void tc01_F02_valid() {
        try {
            assertEquals(0, pizzaService.getTotalAmount(PaymentType.Cash));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    void tc02_F02_nonValid() {
        assertThrows(Exception.class, () -> pizzaService.getTotalAmount(null), "Payment type is invalid!");
    }

    @Test
    void tc03_valid() {
        try {
            pizzaService.addPayment(1, PaymentType.Cash, 100);
            assertEquals(100, pizzaService.getTotalAmount(PaymentType.Cash));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void tc04_valid() {
        try {
            pizzaService.addPayment(1, PaymentType.Cash, 100);
            assertEquals(0, pizzaService.getTotalAmount(PaymentType.Card));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void tc05_valid() {
        try {
            pizzaService.addPayment(1, PaymentType.Cash, 100);
            pizzaService.addPayment(2, PaymentType.Card, 50);
            assertEquals(100, pizzaService.getTotalAmount(PaymentType.Cash));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
