package pizzashop.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;


public class PizzaServiceAddPaymentMockTest {

    @Mock
    private PaymentRepository paymentRepo;

    @InjectMocks
    private PizzaService pizzaService;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    /***** TESTS *****/
    @Test
    public void test01_add_valid_Payment() {
        Payment p = new Payment(1, PaymentType.Card, 20);
        Mockito.when(paymentRepo.getAll()).thenReturn(Arrays.asList(p));
        Mockito.doNothing().when(paymentRepo).add(p);

        try {
            pizzaService.addPayment(1, PaymentType.Card, 20);
            assertEquals(1, paymentRepo.getAll().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test02_add_invalid_Payment() throws Exception{
        Payment p = new Payment(1, PaymentType.Card, -20);

        Mockito.doThrow(RuntimeException.class).when(paymentRepo).add(p);
        assertThrows(Exception.class, () -> pizzaService.add(p), "Amount must be greater than 0!");
    }
}
