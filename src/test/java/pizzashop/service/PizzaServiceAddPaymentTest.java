package pizzashop.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import static org.junit.jupiter.api.Assertions.*;

class PizzaServiceAddPaymentTest {
    private PizzaService pizzaService;

    @BeforeEach
    void setUp() {
        PaymentRepository paymentRepo = new PaymentRepository();
        paymentRepo.clear();
        MenuRepository menuRepo = new MenuRepository();
        pizzaService = new PizzaService(menuRepo, paymentRepo);
    }

    @AfterEach
    void tearDown() {
    }

    // ECP
    @Test
    void tc01ECP_valid() {
        try {
            pizzaService.addPayment(4, PaymentType.Cash, 100); // add payment with valid params
            assertEquals(1, pizzaService.getPayments().size());
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }

    // cannot be implemented
//    @Test
//    void tc02ECP_nonValid() {
//
//    }
//    @Test
//    void tc03ECP_nonValid() {
//
//    }
//    @Test
//    void tc04ECP_nonValid() {
//
//    }

    // BVA
    @Test
    void tc01BVA_nonValid() {
        assertThrows(Exception.class, () -> pizzaService.addPayment(0, PaymentType.Cash, 100), "Table must be between 1-8!");
    }
    @Test
    void tc02BVA_valid() {
        int size = pizzaService.getPayments().size();

        try {
            pizzaService.addPayment(1, PaymentType.Cash, 100);
            assertEquals(size + 1, pizzaService.getPayments().size());
        } catch (Exception e) {
            assertTrue(false, "Table invalid");
            e.printStackTrace();
        }
    }
    @Test
    void tc03BVA_valid() {
        int size = pizzaService.getPayments().size();

        try {
            pizzaService.addPayment(2, PaymentType.Cash, 100);
            assertEquals(size + 1, pizzaService.getPayments().size());
        } catch (Exception e) {
            assertTrue(false, "Table invalid");
            e.printStackTrace();
        }
    }
    @Test
    void tc04BVA_valid() {
        int size = pizzaService.getPayments().size();

        try {
            pizzaService.addPayment(7, PaymentType.Cash, 100);
            assertEquals(size + 1, pizzaService.getPayments().size());
        } catch (Exception e) {
            assertTrue(false, "Could not add payment");
            e.printStackTrace();
        }
    }
    @Test
    void tc05BVA_valid() {
        int size = pizzaService.getPayments().size();

        try {
            pizzaService.addPayment(8, PaymentType.Cash, 100);
            assertEquals(size + 1, pizzaService.getPayments().size());
        } catch (Exception e) {
            assertTrue(false, "Table invalid");
            e.printStackTrace();
        }
    }
    @Test
    void tc06BVA_nonValid() {
        assertThrows(Exception.class, () -> pizzaService.addPayment(9, PaymentType.Cash, 100), "Table must be between 1-8!");
    }
    @Test
    void tc07BVA_nonValid() {
        assertThrows(Exception.class, () -> pizzaService.addPayment(4, PaymentType.Cash, 0), "Amount must be greater than 0!");
    }
    @Test
    void tc08BVA_valid() {
        int size = pizzaService.getPayments().size();

        try {
            pizzaService.addPayment(4, PaymentType.Cash, 1);
            assertEquals(size + 1, pizzaService.getPayments().size());
        } catch (Exception e) {
            assertTrue(false, "Could not add payment");
            e.printStackTrace();
        }
    }
    @Test
    void tc09BVA_valid() {
        int size = pizzaService.getPayments().size();

        try {
            pizzaService.addPayment(4, PaymentType.Cash, 2);
            assertEquals(size + 1, pizzaService.getPayments().size());
        } catch (Exception e) {
            assertTrue(false, "Could not add payment");
            e.printStackTrace();
        }
    }

    // can't be implemented
//    @Test
//    void tc10BVA_valid() {
//    }
//    @Test
//    void tc11BVA_valid() {
//    }
//    @Test
//    void tc12BVA_valid() {
//    }
}