package pizzashop.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import static org.junit.jupiter.api.Assertions.*;

public class IntegrationTestE {
    private PizzaService service;
    private PaymentRepository paymentRepo;
    private MenuRepository menuRepository;


    @BeforeEach
    void setUp() {
        paymentRepo = new PaymentRepository();
        menuRepository = new MenuRepository();
        paymentRepo.clear();
        service = new PizzaService(menuRepository, paymentRepo);
    }

    @Test
    public void tc03_valid() {
        try {
            service.addPayment(8, PaymentType.Cash, 100);
            assertEquals(1, service.getPayments().size());
        } catch (Exception e) {
            assertTrue(false, "Table invalid");
            e.printStackTrace();
        }
    }

    @Test
    public void tc03_non_valid() {
        assertThrows(Exception.class, () -> service.addPayment(4, PaymentType.Cash, 0), "Amount must be greater than 0!");
    }
}
