package pizzashop.service;

import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.List;

public class PizzaService {

    private MenuRepository menuRepo;
    private PaymentRepository payRepo;

    public PizzaService(MenuRepository menuRepo, PaymentRepository payRepo){
        this.menuRepo=menuRepo;
        this.payRepo=payRepo;
    }

    public List<MenuDataModel> getMenuData(){return menuRepo.getMenu();}

    public List<Payment> getPayments(){return payRepo.getAll(); }

    public void addPayment(int table, PaymentType type, double amount) throws Exception {
        if (amount <= 0) {
            throw new Exception("invalid amount");
        }
        if (table < 1 || table > 8) {
            throw new Exception("invalid table number");
        }
        Payment payment= new Payment(table, type, amount);
        payRepo.add(payment);
    }

    public void add(Payment p) throws Exception {
        int table = p.getTableNumber();

        if (p.getAmount() <= 0) {
            throw new Exception("invalid amount");
        }
        if (table < 1 || table > 8) {
            throw new Exception("invalid table number");
        }
        payRepo.add(p);
    }

    public double getTotalAmount(PaymentType type) throws Exception {
        if (type == null) {
            throw new Exception("Payment type is invalid!");
        }
        double total = 0.0f;
        List<Payment> l = getPayments();

        if (l.size() == 0) {
            return total;
        }
        for (Payment p:l) {
            if (p.getType().equals(type))
                total += p.getAmount();
        }

        return total;
    }
}
