package pizzashop.model;

public class PaymentValidator {
    public boolean validate(Payment p) {
        return p.getAmount() > 0;
    }
}
